import React, { useState } from 'react';
import { StyleSheet, Text, View, ScrollView, Image, Dimensions } from 'react-native';

const items = [
  { 
    title: 'Apricots',
    url: 'https://images.unsplash.com/photo-1598136896309-8de1bae8bdd7?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=934&q=80'
  },
  { 
    title: 'Laptop',
    url: 'https://images.unsplash.com/photo-1593642532842-98d0fd5ebc1a?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1950&q=80'
  },
  { 
    title: 'Bike',
    url: 'https://images.unsplash.com/photo-1558981285-501cd9af9426?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1950&q=80'
  }
]

export default function App() {
  const getScrollPosition = (position) => {
    if (position < 1) {
      setActiveElement(0)
    } else if (Math.ceil(position / Dimensions.get('window').width) + 1 <= items.length) {
      setActiveElement(Math.ceil(position / Dimensions.get('window').width))
    } 
  }
  const [activeElement, setActiveElement] = useState(0)
  return (
    <View style={styles.container}>
      <View style={styles.dotContainer}>
        { items.map((item, index) => {
          return (
            <View key={index} style={{ opacity: index === activeElement ? 0.8 : 0.2, ...styles.dot }}></View>
          )
        }) }
      </View>
      <View style={styles.carouselView}>
        <ScrollView
          onScroll={data => { getScrollPosition(data.nativeEvent.contentOffset.x) }}
          horizontal={true}
          contentContainerStyle={{ width: `${100 * items.length}%`, alignItems: 'center' }}
          showsHorizontalScrollIndicator={false}
          scrollEventThrottle={200}
          decelerationRate="fast"
          pagingEnabled
        >
          { items.map((item, index) => {
            return (
              <View key={index} style={{ width: `${100 / items.length}%` }}>
                <Image source={{ uri: item.url }} style={ styles.image } />
              </View>
            )
          }) }
        </ScrollView>
      </View>
      <Text style={ styles.title }>
        { items[activeElement | 0].title }
      </Text>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#1f2933',
    alignItems: 'flex-start',
    justifyContent: 'center',
  },
  carouselView: {
    height: 300
  },
  title: {
    fontWeight: 'bold',
    color: '#f5f7fa',
    fontSize: 28,
    padding: 10
  },
  dot: {
    height: 14,
    width: 14,
    borderRadius: 7,
    margin: 6,
    backgroundColor: 'white'
  },
  dotContainer: {
    width: '100%',
    padding: 10,
    justifyContent: 'flex-end',
    flexDirection: 'row'
  },
  image: {
    height: 300,
    width: '100%'
  }
});
